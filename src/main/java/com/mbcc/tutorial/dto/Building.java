package com.mbcc.tutorial.dto;
import java.util.List;
import java.util.ArrayList;

public class Building {

     List<ResidentInfo> residents = new ArrayList<ResidentInfo> ();

    public void addResident(ResidentInfo info){
        this.residents.add(info);
    }

    public List<ResidentInfo> getResidents(){
        return residents;
    }
}

