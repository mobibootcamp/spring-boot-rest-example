package com.mbcc.tutorial.dto;

public class ResidentInfo {
    private String unitNumber;
    private String firstName;
    private String lastName;
    private long phoneNumber;
    private String emailAdd;
    private String emerContact;
    private String dbKey;

    public ResidentInfo(String fn, String ln, String un){
        this.firstName = fn;
        this.lastName = ln;
        this.unitNumber = un;
    }

    public String getDbKey() {
		return dbKey;
	}

	public void setDbKey(String dbKey) {
		this.dbKey = dbKey;
	}

	public String toString(){
        return this.unitNumber + " " + this.firstName + " " + this.lastName + " " + this.dbKey;
    }

	public String getUnitNumber() {
		return unitNumber;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getEmailAdd() {
		return emailAdd;
	}

    
} 


