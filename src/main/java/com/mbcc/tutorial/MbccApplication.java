package com.mbcc.tutorial;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.DatastoreOptions;


/**
 * Hello world!
 *
 */
@SpringBootApplication
public class MbccApplication {

	Datastore datastore = DatastoreOptions.getDefaultInstance().getService();
	public static void main(String[] args) {
		SpringApplication.run(MbccApplication.class, args);
	}

}
