package com.mbcc.tutorial.persistence;

import java.util.logging.Logger;

import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.DatastoreOptions;
import com.google.cloud.datastore.Entity;
import com.google.cloud.datastore.Key;
import com.google.cloud.datastore.KeyFactory;
import com.mbcc.tutorial.dto.ResidentInfo;



public class DataStoreDao {
	
	Datastore datastore = null;
    private static final Logger log = Logger.getLogger(DataStoreDao.class.getName());

    private KeyFactory keyFactory = null;

    public DataStoreDao() {
        this.datastore = DatastoreOptions.getDefaultInstance().getService();
        keyFactory = datastore.newKeyFactory();
    }

    public DataStoreDao(Datastore datastore) {
        this.datastore = datastore;
        keyFactory = datastore.newKeyFactory();
    }

    public ResidentInfo addResidentInfo(ResidentInfo resident) {

        Key key = datastore.allocateId(keyFactory.setKind("residentInfo").newKey());
        
        Entity residentEntity = Entity.newBuilder(key)
        		.set("firstName", resident.getFirstName())
        		.set("lastName", resident.getLastName())
        		.set("unitNumber",resident.getUnitNumber())
                .build();

                
        datastore.put(residentEntity);
        System.out.println("Inserted resident!");
        
        resident.setDbKey(residentEntity.getKey().toUrlSafe());
        return resident;

    }



}
