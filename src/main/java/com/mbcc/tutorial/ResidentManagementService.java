package com.mbcc.tutorial;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mbcc.tutorial.dto.ResidentInfo;
import com.mbcc.tutorial.persistence.DataStoreDao;


@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/rservice")
public class ResidentManagementService {
	
	 private DataStoreDao dao = new DataStoreDao();
	
	public void setDataStore(DataStoreDao dao) {
        this.dao = dao;
    }

    @GetMapping("/addResident")
    public ResidentInfo addResident(@RequestParam String firstName, @RequestParam String lastName, @RequestParam String unitNumber) {
    	ResidentInfo res = new ResidentInfo(firstName, lastName, unitNumber);
        return this.dao.addResidentInfo(res);
    }


}