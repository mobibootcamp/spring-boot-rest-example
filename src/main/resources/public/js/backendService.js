let backendFunctions = (function() {

	let link = "localhost:8080/";
	
	function addResident(firstName, lastName, unitNumber) {
		let residentInfo = JSON.stringify({
			firstName: firstName,
			lastName: lastName,
			unitNumber: unitNumber
		});


		$.ajax({
			type: 'POST',
			contentType: 'application/json',
			url: link + 'rservice/addResident',
			dataType: "json",
			data: residentInfo,
			success: function(data, textStatus, jqXHR) {
				alert('resident added successfully');
			},
			error: function (jqXHR, exception) {
				var msg = getMessage(jqXHR, exception);
				if(msg != '')
					alert('submit Error: ' +  msg);
			}

		});

	}


	function getMessage(jqXHR, exception){
		let msg = '';
		if (jqXHR.status === 0) {
			msg = 'Not connect.\n Verify Network.';
		} else if (jqXHR.status == 404) {
			msg = 'Requested page not found. [404]';
		} else if (jqXHR.status == 500) {
			msg = 'Internal Server Error [500].';
		} else if (exception === 'parsererror') {
			msg = 'Requested JSON parse failed.';
		} else if (exception === 'timeout') {
			msg = 'Time out error.';
		} else if (exception === 'abort') {
			msg = 'Ajax request aborted.';
		} else {
			msg = 'Uncaught Error.\n' + jqXHR.responseText;
		}
		return msg;
	}

	return {
		addResident: addResident,
		
	}
})();


