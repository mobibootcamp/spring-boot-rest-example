# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.3.0.RELEASE/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.3.0.RELEASE/maven-plugin/reference/html/#build-image)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.5.4/reference/htmlsingle/#boot-features-developing-web-applications)

### Guides
The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)


### Getting the project
Clone the project using git with the below command

`git clone https://gitlab.com/mobibootcamp/spring-boot-rest-example.git`

## Running your application 

Navigate to the project root folder and run the below command:

`mvn spring-boot:run`

## Invoking the Web service
Select WebPreview button on the Google cloud console and click on `Preview on Port 8080` to invoke the Web server

Note everyone's URL will be different; Paste the below URL extension to the one that you have

`rservice/addResident?firstName=John&lastName=Doe&unitNumber=10` to insert a record into the datastore


### Running a Single Test case

To run a single test use - for e.g., ResidentInfo.java filename (can be any test case in the 'test' folder)

`mvn -Dtest=ResidentInfo test`

### Data store
All the datastore activities will be on GCP cloud project mbcc-mailer.


## Tutorial - 2
Now see how Spring Data can encapsulate all the Datastore operations in the below tutorial
https://codelabs.developers.google.com/codelabs/cloud-spring-datastore/#0
